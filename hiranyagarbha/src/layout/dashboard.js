import React from 'react';
import { Link, Route } from 'react-router-dom';
import { Layout, Menu, Icon, Avatar, Badge, Dropdown  } from 'antd';
import { connect } from 'react-redux';
import CreateSMO from '../components/forms/create.SMO';
import { DashboardTable } from '../components/table/table';
import CreateVillage from '../components/forms/create.village';
import CreateANM from '../components/forms/create.ANM';
import { logout } from '../action/login.action';
import {historyHGB} from '../helper/history';

const { Header, Sider, Content } = Layout;


class DashboardLayout extends React.Component {
  state = {
    collapsed: false,
  };

  constructor(props){
    super(props);
    this.logout = this.logout.bind(this);
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }


  logout = () => {
    this.props.dispatch(logout());
  }



  render() {
    console.log(this.props.role);
    return (
      <Layout className='slider-sk'>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
        >
          {/* <div className="logo" /> */}
          <div className='logo'>HGBA  </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={historyHGB.location.pathname}>
            {this.props.role === "bmo" || this.props.role === "cdpo" ? <Menu.Item key="/dashboard">
              <Link to='/dashboard'>
                <Icon type="user" />
                <span>Dashboard</span>
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "bmo" ? <Menu.Item key="/create-users">
              <Link to='/create-users'>
              <Icon type="video-camera" />
              <span>Create Users</span>
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "bmo" ? <Menu.Item key="/add-patient">
              <Link to='/add-patient'>
              <Icon type="video-camera" />
              <span>Add Patients</span>
              </Link>
            </Menu.Item> : ''}
            {/* {this.props.role === "bmo" ? <Menu.Item key="223">
              <Link to='/create-anm'>
              <Icon type="video-camera" />
              <span>Create ANM</span>
              </Link>
            </Menu.Item> : ''} */}
            {this.props.role === "bmo" ? <Menu.Item key="/link-roles">
              <Link to='/link-roles'>
              <Icon type="video-camera" />
              <span>Link Roles</span>
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "cdpo" ? <Menu.Item key="/create-users-icds">
              <Link to='/create-users-icds'>
              <Icon type="video-camera" />
              <span>Create Users</span>
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "cdpo" ? <Menu.Item key="/link-roles-icds">
              <Link to='/link-roles-icds'>
              <Icon type="video-camera" />
              <span>Link Roles</span>
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "cdpo" ? <Menu.Item key="/link-roles">
              <Link to='/link-roles'>
              <Icon type="video-camera" />
              <span>View List</span>  
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "superuser" ? <Menu.Item key="/add-roles">
              <Link to='/add-roles'>
              <Icon type="video-camera" />
              <span>Create Roles </span>  
              </Link>
            </Menu.Item> : ''}
            {this.props.role === "superuser" ? <Menu.Item key="/role-list">
              <Link to='/role-list'>
              <Icon type="video-camera" />
              <span>View List</span>  
              </Link>
            </Menu.Item> : ''}
            {/* <Menu.Item key="3">
              <Icon type="upload" />
              <span>nav 3</span>
            </Menu.Item> */}
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <span style={{fontSize : 14 , fontWeight : 700 , marginRight : 24 ,textTransform: 'capitalize'}} >Hi , <span style={{fontWeight : 200 }}>{this.props.name}</span></span>
            <span style={{fontSize : 14 , fontWeight : 700 , marginRight : 24 ,textTransform: 'capitalize'}} >| Role :<span style={{fontWeight : 200 ,textTransform: 'uppercase'}}> {this.props.role}</span></span>
            <span style={{fontSize : 14 , fontWeight : 700 , marginRight : 24 ,textTransform: 'capitalize'}} >| District : <span style={{fontWeight : 200 }}>{this.props.district}</span></span>
            <span style={{fontSize : 14 , fontWeight : 700 , marginRight : 24 ,textTransform: 'capitalize'}} >| Division : <span style={{fontWeight : 200}}>{this.props.division}</span></span>
            <span style={{ marginRight: 24 , float: 'right', textAlign : 'right',cursor: 'pointer'}} >
                <Badge count={0} onClick={this.logout}><Avatar shape="square" icon="logout" /></Badge>
            </span>
          </Header>
          {/* <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}> */}
              <Route key='1aa'  path = '/create-smo' Component={CreateSMO}/>
              <Route key='1aaw'  path = '/create-anm' Component={CreateANM}/>
              <Route key='1aawe'  path = '/create-village' Component={CreateVillage}/>
          {/* </Content> */}
          {/* <Route key='1aa'  path = '/create-smo' Component={DashboardTable}/> */}
          {this.props.children}
        </Layout>
      </Layout>
    );
  }
}


const mapStateToProps = (state) => {
  return{
      isFetching : state.loginAlerts.isFetching,
      status : state.loginAlerts.status,
      role : state.loginAlerts.role,
      name : state.loginAlerts.name,
      district : state.loginAlerts.district,
      division : state.loginAlerts.division,
  }
}

export default connect(mapStateToProps)(DashboardLayout)
