export const fetchConstant = {
    ABORT : 'ABORT',
    ERROR : 'ERROR',
    START : 'START',
    UPDATE : 'UPDATE'
}

export const listFetchConstant = {
    LIST_FETCH_REQUEST : 'LIST_FETCH_REQUEST',
    LIST_FETCH_SUCCESS : 'LIST_FETCH_SUCCESS',
    LIST_FETCH_ERROR : 'LIST_FETCH_ERROR'
}