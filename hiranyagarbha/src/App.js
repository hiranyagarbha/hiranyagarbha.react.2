import React, { Component } from 'react';
import './App.css';
import { LoginView } from './views/login/login';
import { Router, Route , Switch } from 'react-router-dom';
import {historyHGB} from './helper/history';
import { DashboardView } from './views/dashboard/dashboard';
import { PrivateRoute } from './router/private.route';
import { CreateSMOView } from './views/dashboard/create.users';
import { CreateAnganbadiView } from './views/dashboard/create.anganbadi';
import { LinkSmoAnmVillageView } from './views/dashboard/link.smo.anm.village';
import { EditableTable } from './components/table/link.table';
import { CreateUsersIcdsView } from './views/dashboard/create.users.icds';
import { LinkSupervisorVillageAnganbadiWorkersView } from './views/dashboard/link.supervisor.village.anganbadi.workers';
import { CreatePatientView } from './views/dashboard/add.patient';
import {AddRoleView} from './views/dashboard/add.roles';
import { RoleListView } from './views/dashboard/role.list';


class App extends Component {
  render() {
    return (
      <Router history={historyHGB}>
        <Switch>
          <Route path='/dashboard' component = {DashboardView} />
          <Route exact path='/login' component = {LoginView} />
          {/* //Testing Routing links } */}
          <Route path='/ed' component = {EditableTable} />
          {/* //Testing Routing links } */}
          <Route path='/link-roles' component = {LinkSmoAnmVillageView} />
          <PrivateRoute path= "/create-users" component = {CreateSMOView} />
          <PrivateRoute path= "/create-anganbadi" component = {CreateAnganbadiView} />
          <PrivateRoute path= "/create-users-icds" component = {CreateUsersIcdsView} />
          <PrivateRoute path= "/link-roles-icds" component = {LinkSupervisorVillageAnganbadiWorkersView} />
          <PrivateRoute path= "/add-patient" component = {CreatePatientView} />
          <PrivateRoute path= "/add-roles" component = {AddRoleView} />
          <PrivateRoute path= "/role-list" component = {RoleListView} />
          <Route component = {LoginView} />
        </Switch>
      </Router>
    );
  }
}

export default App;
