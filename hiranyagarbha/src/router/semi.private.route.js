import React from 'react';
import { Route , Redirect} from 'react-router-dom';
import {store} from '../helper/store';



export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => ( store.getState().loginAlerts.status === true
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
);

const unsubscribe = store.subscribe(PrivateRoute)
unsubscribe()