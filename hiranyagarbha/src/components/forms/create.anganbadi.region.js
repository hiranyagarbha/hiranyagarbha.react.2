import React from 'react';
import { Form, Icon, Input, Button,Layout } from 'antd';
import {connect} from 'react-redux';
import { UserCreation } from '../../action/user.create.action';
import { AuthHeader } from '../../helper/auth.token';

const {Content} = Layout
const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class HorizontalLoginForm extends React.Component {
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(JSON.stringify(values), AuthHeader())
        let url = 'https://enduring-grid-209608.appspot.com/add_village/'
        this.props.dispatch(UserCreation(url ,JSON.stringify(values), AuthHeader()))
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const nameError = isFieldTouched('name') && getFieldError('name');
    return (
      <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
      <h2>Create Anganbadi </h2>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <FormItem
            validateStatus={nameError ? 'error' : ''}
            help={nameError || ''}
          >
          <h4>Anganbadi Region Name</h4>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input Village name!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder=" Village" />
            )}
          </FormItem>
          <FormItem>
          <h4>Submit</h4>
            <Button
              type="primary"
              htmlType="submit"
              disabled={hasErrors(getFieldsError())}
            >
              Create Anganbadi
            </Button>
          </FormItem>
        </Form>
        <h5>* Anganbadi Region are created for Linking </h5>
      </Content>
    );
  }
}

const CreateAnganbadiRegion = Form.create()(HorizontalLoginForm);

const mapStateToProps = (state) => {
  return{
      isFetching : state.loginAlerts.isFetching,
  }
}

export default connect(mapStateToProps)(CreateAnganbadiRegion)
