import React from 'react';
import { Layout, Form, Button, Row, Col, Select,Spin} from 'antd';
import { listFetch, fetchIfNeeded } from '../../action/list.fetch.action';
import { AuthHeader } from '../../helper/auth.token';
import { connect } from 'react-redux';
import { link } from '../../action/link.action';


const FormItem = Form.Item;
const { Content } = Layout
const Option = Select.Option

class LinkAnmVillage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            anm : null,
            village : null,
            linked : Array()
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChange2 = this.handleChange2.bind(this)
        this.handlesubmit = this.handlesubmit.bind(this)
    }


    // componentWillMount(){
    //     let url = 'https://enduring-grid-209608.appspot.com/all_dropdown'
    //     const {dispatch} = this.props
    //     dispatch(listFetch(url , AuthHeader()))
    // }

    handlesubmit = (e) => {
        e.preventDefault();
        let values = this.state
        let url = "https://enduring-grid-209608.appspot.com/create_link_anm_village"
        // this.props.dispatch(fetchIfNeeded(url , AuthHeader()))
        this.props.dispatch(link(url,values,AuthHeader()))
        console.log(this.state);
        // this.setState({
        //     anm : null,
        //     villages : null,
        //     linkedList : values
        // })
        console.log(this.state);
    }

    handleChange(value){
        this.setState({
            anm : value,
        });
    }
    handleChange2(value){
        this.setState({
            village : value,
        });
    }

    render(){
        console.log('village anm list', this.props.isFetching)
        return(
            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            {this.props.isFetching ? <div className='spin-sk'><Spin/></div> :
                <Row>
                <h2>Link ANM with Village </h2>
                <Form layout="inline" onSubmit={this.handlesubmit}>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select ANM </h4>
                        <Select onChange={this.handleChange} style={{width : "250px"}} placeholder="Select a SMO">
                            {this.props.anmList.map((item) =>(
                                <Option key={item} value={item}>{item}</Option>
                            ))}
                        </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select Village</h4>
                    <Select
                    mode="multiple"
                    style={{width : "250px"}}
                    placeholder="Please select Villages"
                    // defaultValue={['a10', 'c12']}
                    onChange={this.handleChange2}>
                        {this.props.villageList.map((item) =>(
                            <Option key={item}>{item}</Option>
                        ))}
                    </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Submit</h4>
                        <Button
                        type="primary"
                        htmlType="submit"
                        >
                        Link ANM with VILLAGEs
                        </Button>
                    </FormItem>
                    </Col>
                </Form>
                </Row>
            }
            </Content>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        isFetching : state.listFetchReducer.isFetching,
        anmList : state.listFetchReducer.anm_list,
        villageList : state.listFetchReducer.village_list
    }
  }
  
  export default connect(mapStateToProps)(LinkAnmVillage)