import React from 'react';
import { Form, Icon, Input, Button, Layout,DatePicker ,Select,Checkbox,InputNumber, Row,Col} from 'antd';
import {connect} from 'react-redux';
import { AuthStr, AuthHeader } from '../../helper/auth.token';
import { addPatient } from '../../action/add.patient.action';

const { TextArea } = Input;
const {Content} = Layout
const Option = Select.Option;
const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class HorizontalLoginForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            highriskchecker : false,
        }
    }


  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  onChangeCheckbox = (e) => {
      console.log(e);
      this.setState({
          highriskchecker : true,
      })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
        console.log(values);
      if (!err) {
        let url = 'https://enduring-grid-209608.appspot.com/patient_full_entry/'
        this.props.dispatch(addPatient(url , values , AuthHeader()))
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const patientnameError = isFieldTouched('patient_name') && getFieldError('patient_name');
    const husbandnameError = isFieldTouched('husband_name') && getFieldError('husband_name');
    const aadharError = isFieldTouched('aadhar_number') && getFieldError('aadhar_number');
    const ageError = isFieldTouched('age') && getFieldError('age');
    const dobError = isFieldTouched('date_of_birth') && getFieldError('date_of_birth');
    const mobileError = isFieldTouched('mobile_number') && getFieldError('mobile_number');
    const malechildError = isFieldTouched('male_child') && getFieldError('male_child');
    const femalechildError = isFieldTouched('female_child') && getFieldError('female_child');
    const economicstatusError = isFieldTouched('economic_status') && getFieldError('economic_status');
    const castError = isFieldTouched('cast') && getFieldError('cast');
    const religionError = isFieldTouched('relegion') && getFieldError('relegion');
    const lmpError = isFieldTouched('lmp_date') && getFieldError('lmp_date');
    const weightError = isFieldTouched('weight') && getFieldError('weight');
    const eddError = isFieldTouched('edd_date') && getFieldError('edd_date');
    const officersError = isFieldTouched('officer') && getFieldError('officer');
    const anganbadiError = isFieldTouched('agbdi_name') && getFieldError('agbdi_name');
    const bp1Error = isFieldTouched('bp1') && getFieldError('bp1');
    const bp2Error = isFieldTouched('bp2') && getFieldError('bp2');
    const abortionError = isFieldTouched('abortion_miscarriage') && getFieldError('abortion_miscarriage');
    const pregnancyError = isFieldTouched('pregnancy_number') && getFieldError('pregnancy_number');
    const sugarError = isFieldTouched('sugar') && getFieldError('sugar');
    const haemoglobinError = isFieldTouched('haemoglobin') && getFieldError('haemoglobin');
    const highriskError = isFieldTouched('high_risk') && getFieldError('high_risk');
    const highriskcheckError = isFieldTouched('high_risk_check') && getFieldError('high_risk_check');
    
    return (
        <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            <Row>
            <Col xs={{span : 2,offset: 0}} sm={{span : 4, offset: 0}} md={{span : 6, offset: 0}} lg={{span : 8, offset: 0}} xl={{span : 8, offset: 0}}>
            <h2>Add Previous Patients Records</h2>
            <Form onSubmit={this.handleSubmit}>
                <FormItem
                validateStatus={patientnameError ? 'error' : ''}
                help={patientnameError || ''}
                >
                <h4>Patient Name</h4>
                {getFieldDecorator('patient_name', {
                    rules: [{ required: true, message: 'Please input Patient Name' }],
                })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Patient Name" />
                )}
                </FormItem>
                <FormItem
                validateStatus={husbandnameError ? 'error' : ''}
                help={husbandnameError || ''}
                >
                <h4>Husband Name</h4>
                {getFieldDecorator('husband_name', {
                    rules: [{ required: true, message: 'Please input Husband Name!' }],
                })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Husband Name" />
                )}
                </FormItem>
                <FormItem
                    validateStatus={aadharError? 'error' : ''}
                    help={aadharError|| ''}
                >
                <h4>Aadhar No</h4>
                    {getFieldDecorator('aadhar_number', {
                    rules: [{ required: true, message: 'Please input patient Aadhar No!' }],
                    })(
                    <InputNumber style={{width: "100%"}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Aadhar No" />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={mobileError ? 'error' : ''}
                    help={mobileError || ''}
                >
                <h4>Mobile No</h4>
                    {getFieldDecorator('mobile_number', {
                    rules: [{ required: true, message: 'Please input your mobile!' }],
                    })(
                    <InputNumber style={{width: "100%"}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Mobile" />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={dobError? 'error' : ''}
                    help={dobError|| ''}
                >
                <h4>Date of Birth</h4>
                    {getFieldDecorator('date_of_birth', {
                    rules: [{ required: true, message: 'Please input Patient DOB!' }],
                    })(
                    <DatePicker style={{width: "100%"}} placeholder="Select DOB"/>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={ageError ? 'error' : ''}
                    help={ageError || ''}
                >
                <h4>Patient Age</h4>
                    {getFieldDecorator('age', {
                    rules: [{ required: true, message: 'Please input Patient Age!' }],
                    })(
                    <InputNumber style={{width: "100%"}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Age" />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={malechildError ? 'error' : ''}
                    help={malechildError || ''}
                >
                <h4>Male Child</h4>
                    {getFieldDecorator('male_child', {
                    rules: [{ required: true, message: 'Please input Nos' }],
                    })(
                    <InputNumber style={{width: "100%"}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Male Nos" />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={femalechildError ? 'error' : ''}
                    help={femalechildError || ''}
                >
                <h4>Female Child</h4>
                    {getFieldDecorator('female_child', {
                    rules: [{ required: true, message: 'Please input Nos' }],
                    })(
                    <InputNumber style={{width: "100%"}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Female Nos" />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={economicstatusError ? 'error' : ''}
                    help={economicstatusError || ''}
                >
                <h4>Economic Status</h4>
                    {getFieldDecorator('economic_status', {
                    rules: [{ required: true, message: 'Please slect required field' }],
                    })(
                        <Select defaultValue="lucy" placeholder="Select Economic Status">
                            <Option value="jack">APL</Option>
                            <Option value="lucy">BPL</Option>
                            <Option value="Yiminghe">Don't Know</Option>
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={castError ? 'error' : ''}
                    help={castError || ''}
                >
                <h4>Cast</h4>
                    {getFieldDecorator('cast', {
                    rules: [{ required: true, message: 'Please select cast' }],
                    })(
                        <Select defaultValue="lucy"  placeholder="Select Economic Status">
                            <Option value="jack">SC</Option>
                            <Option value="lucy">ST</Option>
                            <Option value="Yiminghe">Others</Option>
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={religionError ? 'error' : ''}
                    help={religionError || ''}
                >
                <h4>Religion</h4>
                    {getFieldDecorator('relegion', {
                    rules: [{ required: true, message: 'Please Select Religion' }],
                    })(
                        <Select defaultValue="lucy" placeholder="Select Religion">
                            <Option value="Hindu">Hindu</Option>
                            <Option value="Muslim">Muslim</Option>
                            <Option value="Sikh">Sikh</Option>
                            <Option value="Christian">Christian</Option>
                            <Option value="Others">Others</Option>
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={lmpError ? 'error' : ''}
                    help={lmpError || ''}
                >
                <h4>LMP Date</h4>
                    {getFieldDecorator('lmp_date', {
                    rules: [{ required: true, message: 'Please LMP Date' }],
                    })(
                    <DatePicker style={{width: "100%"}} placeholder="Select Date"/>
                    )}
                </FormItem>
                <FormItem
                validateStatus={weightError ? 'error' : ''}
                help={weightError || ''}
                >
                <h4>Weight</h4>
                {getFieldDecorator('weight', {
                    rules: [{ required: true, message: 'Please input weight' }],
                })(
                    <InputNumber style={{width: "100%"}} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Weight" />
                )}
                </FormItem>
                <FormItem
                    validateStatus={eddError ? 'error' : ''}
                    help={eddError || ''}
                >
                <h4>EDD Date</h4>
                    {getFieldDecorator('edd_date', {
                    rules: [{ required: true, message: 'Please select date!' }],
                    })(
                    <DatePicker style={{width: "100%"}} placeholder="Select Date"/>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={officersError ? 'error' : ''}
                    help={officersError || ''}
                >
                <h4>Officers/Workers</h4>
                    {getFieldDecorator('officer', {
                    rules: [{ required: true, message: 'Please select officers!' }],
                    })(
                        <Select mode="multiple" placeholder="Select Officers">
                            {this.props.smo.map((item) =>(
                                <Option key={item} value={item}>{item}</Option>
                            ))}
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={anganbadiError ? 'error' : ''}
                    help={anganbadiError || ''}
                >
                <h4>Anganbadi Name</h4>
                    {getFieldDecorator('agbdi_name', {
                    rules: [{ required: true, message: 'Please Select Anganbadi' }],
                    })(
                        <Select placeholder="Select Anganbadi">
                            {this.props.anganbadi.map((item) =>(
                                <Option key={item} value={item}>{item}</Option>
                            ))}
                        </Select>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={abortionError ? 'error' : ''}
                    help={abortionError || ''}
                >
                <h4>Abortion</h4>
                    {getFieldDecorator('abortion_miscarriage', {
                    valuePropName:"unchecked",
                    initialValue: true,
                    })(
                        <Checkbox style={{width: "100%"}} >Checkbox</Checkbox>
                    )}
                </FormItem>
                <FormItem
                    validateStatus={bp1Error ? 'error' : ''}
                    help={bp1Error || ''}
                >
                <h4>Blood Pressure</h4>
                    <div>
                    {getFieldDecorator('bp1', {
                    rules: [{ required: true, message: 'Please input Values!' }],
                    })(
                    <InputNumber
                    type="number"
                    style={{marginRight: "10px"}}
                    />
                    )}
                    {getFieldDecorator('bp2', {
                    rules: [{ required: true, message: 'Please input Values!' }],
                    })(
                    <InputNumber
                    type="number"
                    />
                    )}
                    </div>
                </FormItem>
                <FormItem
                    validateStatus={sugarError ? 'error' : ''}
                    help={sugarError || ''}
                >
                <h4>Sugar</h4>
                    {getFieldDecorator('sugar', {
                    rules: [{ required: true, message: 'Please input value!' }],
                    })(
                    <InputNumber
                        style={{width: "100%"}}
                        type="number"
                    />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={haemoglobinError ? 'error' : ''}
                    help={haemoglobinError || ''}
                >
                <h4>Haemoglobin</h4>
                    {getFieldDecorator('haemoglobin', {
                    rules: [{ required: true, message: 'Please input value!' }],
                    })(
                    <InputNumber
                        style={{width: "100%"}}
                        type="number"
                    />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={pregnancyError ? 'error' : ''}
                    help={pregnancyError || ''}
                >
                <h4>Pregnancy No</h4>
                    {getFieldDecorator('pregnancy_number', {
                    rules: [{ required: true, message: 'Please input Nos!' }],
                    })(
                    <InputNumber
                        style={{width: "100%"}}
                        type="number"
                        // min={0}
                        // max={100}
                    />
                    )}
                </FormItem>
                <FormItem
                    validateStatus={highriskcheckError ? 'error' : ''}
                    help={highriskcheckError || ''}
                >
                <h4>High Risk or Not ?</h4>
                    {getFieldDecorator('high_risk_check', {
                    valuePropName:"unchecked",
                    initialValue: true,
                    })(
                        <Checkbox style={{width: "100%"}} onChange={this.onChangeCheckbox} >High Risk</Checkbox>
                    )}
                </FormItem>
                { this.state.highriskchecker === true ?
                <FormItem
                    validateStatus={highriskError ? 'error' : ''}
                    help={highriskError || ''}
                >
                <h4>High Risk Reasons </h4>
                    {getFieldDecorator('high_risk', {
                    rules: [{ required: true, message: 'Please Select Reasons!' }],
                    })(
                        <Select defaultValue="lucy" mode="multiple" placeholder="Select Reason">
                            <Option value="High BP">High BP</Option>
                            <Option value="Convulsions">Convulsions</Option>
                            <Option value="Vaginal Bleeding">Vaginal Bleeding</Option>
                            <Option value="Foul Smell Discharge">Foul Smell Discharge</Option>
                            <Option value="Severe Anemia">Severe Anemia</Option>
                            <Option value="Diabetes">Diabetes</Option>
                            <Option value="Twins">Twins</Option>
                            <Option value="Any Others">Any Others</Option>
                        </Select>
                    )}
                </FormItem> : ""
                }

                <FormItem
                    // validateStatus={mobileError ? 'error' : ''}
                    // help={mobileError || ''}
                >
                <h4>Dietary Advice</h4>
                    {getFieldDecorator('dietary_advice', {
                    rules: [{ required: true, message: 'Input your Advices!' }],
                    })(
                        <TextArea placeholder="Advice / Notes " autosize={{ minRows: 2, maxRows: 6 }} />
                    )}
                </FormItem>
                {/* <FormItem>
                    {getFieldDecorator('role' ,{
                    initialValue : 'icds'
                    })(
                    <Input style={{display : 'none'}} disabled={true} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text" placeholder="Password"/>
                    )}
                </FormItem> */}
                <FormItem>
                <h4>Submit</h4>
                <Button
                    style={{width: "100%"}}
                    type="primary"
                    htmlType="submit"
                    disabled={hasErrors(getFieldsError())}
                >
                    Add Patient Details
                </Button>
                </FormItem>
            </Form>
            <h5>* Completing all Fields is mandadatory.</h5>
            </Col>
            <Col xs={{span : 2,offset: 1}} sm={{span : 4, offset: 1}} md={{span : 6, offset: 1}} lg={{span : 8, offset: 1}} xl={{span : 8, offset: 1}}>
            saasdnasn
            </Col>
            </Row>

        </Content>
    );
  }
}

const CreatePatients = Form.create()(HorizontalLoginForm);

const mapStateToProps = (state) => {
    return{
        isFetching : state.loginAlerts.isFetching,
        smo : state.loginAlerts.smo,
        anganbadi : state.loginAlerts.anganbadi,
    }
}
  
export default connect(mapStateToProps)(CreatePatients)
  