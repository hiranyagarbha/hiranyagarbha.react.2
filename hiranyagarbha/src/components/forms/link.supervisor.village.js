import React from 'react';
import { Layout, Form, Button, Row, Col, Select,Spin} from 'antd';
import { AuthHeader } from '../../helper/auth.token';
import { connect } from 'react-redux';
import { link } from '../../action/link.action';
import { listFetch } from '../../action/list.fetch.action';


const FormItem = Form.Item;
const { Content } = Layout
const Option = Select.Option

class LinkSupervisorVillage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            sup : null,
            village : null,
            linked : Array()
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChange2 = this.handleChange2.bind(this)
        this.handlesubmit = this.handlesubmit.bind(this)
    }


    componentWillMount(){
        let url = 'https://enduring-grid-209608.appspot.com/icds_dropdown'
        this.props.dispatch(listFetch(url , AuthHeader()))
    }

    handlesubmit = (e) => {
        e.preventDefault();
        let values = this.state
        let url = "https://enduring-grid-209608.appspot.com/create_link_sup_village"
        // this.props.dispatch(fetchIfNeeded(url , AuthHeader()))
        this.props.dispatch(link(url,values,AuthHeader()))
        console.log(this.state);
        this.setState({
            sup : null,
            villages : null,
            linkedList : values
        })
        console.log(this.state);
    }

    handleChange(value){
        this.setState({
            sup : value,
        });
    }
    handleChange2(value){
        this.setState({
            villages : value
        });
    }

    render(){
        console.log('Supervisor village list', this.props.isFetching)
        return(
            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            {this.props.isFetching ? <div className='spin-sk'><Spin/></div> :
                <Row>
                <h2>Link Supervisor with Village </h2>
                <Form layout="inline" onSubmit={this.handlesubmit}>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select Supervisor </h4>
                        <Select onChange={this.handleChange} style={{width : "250px"}} placeholder="Select a SMO">
                            {this.props.supervisorList.map((item) =>(
                                <Option key={item} value={item}>{item}</Option>
                            ))}
                        </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select Village</h4>
                    <Select
                    mode="multiple"
                    style={{width : "250px"}}
                    placeholder="Please select Villages"
                    // defaultValue={['a10', 'c12']}
                    onChange={this.handleChange2}>
                        {this.props.villagesIcdsList.map((item) =>(
                            <Option key={item}>{item}</Option>
                        ))}
                    </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Submit</h4>
                        <Button
                        type="primary"
                        htmlType="submit"
                        >
                        Link Supervisor with Villages
                        </Button>
                    </FormItem>
                    </Col>
                </Form>
                </Row>
            }
            </Content>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        isFetching : state.listFetchReducer.isFetching,
        supervisorList : state.listFetchReducer.supervisor,
        villagesIcdsList : state.listFetchReducer.villages_icds
    }
  }
  
  export default connect(mapStateToProps)(LinkSupervisorVillage)