import React from 'react';
import { Input, Icon,Steps, Button, message, Layout , Select, Radio , Form} from 'antd';
import {AuthHeader } from '../../helper/auth.token';
import { AddRoles } from "../../action/add.roles.action";
import {connect} from 'react-redux';

const Step = Steps.Step;
const {Content} = Layout;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { TextArea } = Input;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

const steps = [{
  title: 'Select State',
}, {
  title: 'Select Divisions & Roles',
}, {
  title: 'Enter User Details',
}];

class Role extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      state : null,
      division : null,
      district : null,
      block : null ,
      role : null,
      username : null ,
      mobile : null ,
      password : null,
    };
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChangeState = this.handleChangeState.bind(this)
    this.handleChangeDistrict = this.handleChangeDistrict.bind(this);
    this.handleChangeBlock = this.handleChangeBlock.bind(this);
    this.handleChangeRole = this.handleChangeRole.bind(this);
    this.handleChangeusername = this.handleChangeusername.bind(this);
    this.handleChangemobile = this.handleChangemobile.bind(this);
    this.handleChangepassword = this.handleChangepassword.bind(this);
    this.handleChangeDivision = this.handleChangeDivision.bind(this);
  }

  handleChangeState = (value) => {
    this.setState({
      state : value,
    })
    console.log(value);
  }
  handleChangeDivision = (value) => {
    this.setState({
      division : value,
    })
    console.log(value);
  }
  handleChangeDistrict = (value) => {
    this.setState({
      district : value,
    })
    console.log(value);
  }
  handleChangeBlock = (value) => {
    this.setState({
      block : value,
    })
    console.log(value);
  }
  handleChangeRole = (value) => {
    this.setState({
      role : value.target.value,
    })
    console.log(value);
  }
  handleChangeusername = (value) => {
    this.setState({
      username : value.target.value,
    })
    console.log(value);
  }
  handleChangemobile = (value) => {
    this.setState({
      mobile : value.target.value,
    })
    console.log(value);
  }
  handleChangename = (value) => {
    this.setState({
      name : value.target.value,
    })
    console.log(value);
  }
  handleChangepassword = (value) => {
    this.setState({
      password : value.target.value,
    })
    console.log(value);
    console.log(this.state);
  }

  handleSubmit = () => {
    // this.props.form.validateFields((err, values) => {
    //     console.log(values);
    //   if (!err) {
    //     let url = 'https://enduring-grid-209608.appspot.com/patient_full_entry/'
    //     this.props.dispatch(AddRoles(url , values , AuthHeader()))
    //     console.log('Received values of form: ', values);
    //   }
    // });
    let values = this.state
    let url = 'https://enduring-grid-209608.appspot.com/users/users'
    this.props.dispatch(AddRoles(url , values , AuthHeader()))
    console.log('Received values of form: ', this.state);
  }


  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state; 
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    const usernameError = isFieldTouched('username') && getFieldError('username');
    const nameError = isFieldTouched('name') && getFieldError('name');
    const stateError = isFieldTouched('state') && getFieldError('state');
    const divisionError = isFieldTouched('division') && getFieldError('division');
    const districtError = isFieldTouched('district') && getFieldError('district');
    const blockError = isFieldTouched('block') && getFieldError('block');
    const roleError = isFieldTouched('role') && getFieldError('role');
    const mobileError = isFieldTouched('mobile') && getFieldError('mobile');
    const passwordError = isFieldTouched('password') && getFieldError('password');

    return (
      <Content style={{ margin: '3% 3%', padding: 24, background: '#fff' }} >
      <h2 style={{textAlign : 'center'}} >Create CDPO and BMO </h2>
      <Form onSubmit={this.handleSubmit}>
      <div>
        <Steps current={current}>
          {steps.map(item => <Step key={item.title} title={item.title} />)}
        </Steps>
        <div className="steps-content">
          {current === 0 ? 
            <FormItem
            validateStatus={stateError ? 'error' : ''}
            help={stateError || ''}
            >
            <h3>Select State</h3>
            {getFieldDecorator('state', {
                rules: [{ required: true, message: 'Please select state!' }],
            })(
            <Select placeholder="Select State"  style={{width : '60%'}} onChange={this.handleChangeState}>
              <Option value="Madhya Pradesh">Madhya Pradesh</Option>
            </Select>
          )}
          </FormItem> :
          ""}
          {current === 1 ? 
          <blockquote>
          <FormItem
            validateStatus={divisionError ? 'error' : ''}
            help={divisionError || ''}
          >
            <h3>Select Division</h3>
            {getFieldDecorator('division', {
                rules: [{ required: true, message: 'Please input Patient Name' }],
            })(
            <Select placeholder="Select Division" style={{width : '60%'}}  onChange={this.handleChangeDivision}>
              <Option value="division1">division1</Option>
              <Option value="division2">division2</Option>
              <Option value="division3">division3</Option>
            </Select>
          )}
          </FormItem>
          <FormItem
            validateStatus={districtError ? 'error' : ''}
            help={districtError || ''}
          >
          <h3>Select District</h3>
            {getFieldDecorator('district', {
                rules: [{ required: true, message: 'Please input Patient Name' }],
            })(
            <Select placeholder="Select Division"  style={{width : '60%'}} onChange={this.handleChangeDistrict}>
              <Option value="district1">district1</Option>
              <Option value="district2">district2</Option>
              <Option value="district3">district3</Option>
            </Select>
          )}
          </FormItem>
          <FormItem
            validateStatus={blockError ? 'error' : ''}
            help={blockError || ''}
          >
          <h3>Select Block</h3>
            {getFieldDecorator('block', {
                rules: [{ required: true, message: 'Please input Patient Name' }],
            })(
            <Select placeholder="Select Division"  style={{width : '60%'}} onChange={this.handleChangeBlock}>
              <Option value="block1">block1</Option>
              <Option value="block2">block2</Option>
              <Option value="block3">block3</Option>
            </Select>
          )}
          </FormItem>
          <FormItem
            validateStatus={roleError ? 'error' : ''}
            help={roleError || ''}
          >
          <h3>Select Role</h3>
            {getFieldDecorator('role', {
                rules: [{ required: true, message: 'Please input Patient Name' }],
            })(
            <RadioGroup onChange={this.handleChangeRole}>
              <Radio value="cdpo">CDPO</Radio>
              <Radio value="bmo">BMO</Radio>
            </RadioGroup>
          )}
          </FormItem> 
          </blockquote> :
          ""}
          {current === steps.length -1 ? 
          <blockquote>
            <FormItem
            validateStatus={nameError ? 'error' : ''}
            help={nameError || ''}
            >
            <h3>Name</h3>
            {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input User Name' }],
            })(
              <Input style={{width : '60%'}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} onChange={this.handleChangename} placeholder="Name" />
            )}
            </FormItem>
            <FormItem
            validateStatus={mobileError ? 'error' : ''}
            help={mobileError || ''}
            >
            <h3>Mobile No</h3>
            {getFieldDecorator('mobile', {
                rules: [{ required: true, message: 'Please input Mobile No' }],
            })(
              <Input style={{width : '60%'}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} onChange={this.handleChangemobile} placeholder="Mobile No" />
            )}
            </FormItem>
            <FormItem
            validateStatus={usernameError ? 'error' : ''}
            help={usernameError || ''}
            >
            <h3>User Name</h3>
            {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Please input User Name' }],
            })(
              <Input style={{width : '60%'}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} onChange={this.handleChangeusername} placeholder="User Name" />
            )}
            </FormItem>
            <FormItem
            validateStatus={passwordError ? 'error' : ''}
            help={passwordError || ''}
            >
            <h3>Password</h3>
            {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input password' }],
            })(
              <Input style={{width : '60%'}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} onChange={this.handleChangepassword} placeholder="User Name" />
            )}
            </FormItem>
          </blockquote> :
          ""}
        </div>
        <div className="steps-action">
          {
            current < steps.length - 1
            && <Button type="primary" onClick={() => this.next()}>Next</Button>
          }
          {
            current === steps.length - 1
            && <Button onClick={this.handleSubmit} type="primary">Add User</Button>
          }
          {
            current > 0
            && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
            )
          }
        </div>
      </div>
      </Form>
      </Content>
    );
  }
}


const AddRole = Form.create()(Role);

const mapStateToProps = (state) => {
  return{
      isFetching : state.loginAlerts.isFetching,
  }
}

export default connect(mapStateToProps)(AddRole)