import React from 'react';
import { Layout, Form, Button, Row, Col, Select,Spin} from 'antd';
import { listFetch } from '../../action/list.fetch.action';
import { AuthHeader } from '../../helper/auth.token';
import { connect } from 'react-redux';
import { link } from '../../action/link.action';

const FormItem = Form.Item;
const { Content } = Layout
const Option = Select.Option

class LinkSmoAnm extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            smo : null,
            anm : null,
            linked : Array()
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChange2 = this.handleChange2.bind(this)
        this.handlesubmit = this.handlesubmit.bind(this)
    }


    componentWillMount(){
        let url = 'https://enduring-grid-209608.appspot.com/all_dropdown'
        this.props.dispatch(listFetch(url , AuthHeader()))
    }

    handlesubmit = (e) => {
        e.preventDefault();
        let values = this.state
        let url = "https://enduring-grid-209608.appspot.com/create_link_smo_anm"
        this.props.dispatch(link(url,values,AuthHeader()))
        console.log(this.state);
        this.setState({
            smo : null,
            anm : null,
            linkedList : values
        })
        console.log(this.state);
    }

    handleChange(value){
        this.setState({
            smo : value,
        });
    }
    handleChange2(value){
        this.setState({
            anm : value
        });
    }

    render(){
        console.log('smolist',this.props.smoList, this.props.isFetching, this.state.selectedSMO)
        return(
            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            {this.props.isFetching ? <div className='spin-sk'><Spin/></div> :
                <Row>
                <h2>Link SMO with ANM </h2>
                <Form layout="inline" onSubmit={this.handlesubmit}>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select SMO </h4>
                        <Select onChange={this.handleChange} style={{width : "250px"}} placeholder="Select a SMO">
                            {this.props.smoList.map((item) =>(
                                <Option key={item} value={item}>{item}</Option>
                            ))}
                        </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select ANMs' </h4>
                    <Select
                    mode="multiple"
                    style={{width : "250px"}}
                    placeholder="Please select"
                    // defaultValue={['a10', 'c12']}
                    onChange={this.handleChange2}>
                        {this.props.anmList.map((item) =>(
                            <Option key={item}>{item}</Option>
                        ))}
                    </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Submit</h4>
                        <Button
                        type="primary"
                        htmlType="submit"
                        >
                        Link SMO with ANMs
                        </Button>
                    </FormItem>
                    </Col>
                </Form>
                </Row>
            }
            </Content>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        isFetching : state.listFetchReducer.isFetching,
        smoList : state.listFetchReducer.smo_list,
        anmList : state.listFetchReducer.anm_list
    }
  }
  
  export default connect(mapStateToProps)(LinkSmoAnm)