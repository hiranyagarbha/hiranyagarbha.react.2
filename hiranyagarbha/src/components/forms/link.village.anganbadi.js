import React from 'react';
import { Layout, Form, Button, Row, Col, Select,Spin} from 'antd';
import { listFetch, fetchIfNeeded } from '../../action/list.fetch.action';
import { AuthHeader } from '../../helper/auth.token';
import { connect } from 'react-redux';
import { link } from '../../action/link.action';


const FormItem = Form.Item;
const { Content } = Layout
const Option = Select.Option

class LinkVillageAnganbadi extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            agbdis : null,
            village : null,
            linked : Array()
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChange2 = this.handleChange2.bind(this)
        this.handlesubmit = this.handlesubmit.bind(this)
    }


    // componentWillMount(){
    //     let url = 'https://enduring-grid-209608.appspot.com/all_dropdown'
    //     const {dispatch} = this.props
    //     dispatch(listFetch(url , AuthHeader()))
    // }

    handlesubmit = (e) => {
        e.preventDefault();
        let values = this.state
        let url = " https://enduring-grid-209608.appspot.com/create_link_village_agbdi"
        // this.props.dispatch(fetchIfNeeded(url , AuthHeader()))
        this.props.dispatch(link(url,values,AuthHeader()))
        console.log(this.state);
        this.setState({
            agbdis : null,
            village : null,
            linkedList : values
        })
        console.log(this.state);
    }

    handleChange(value){
        this.setState({
            agbdis : value,
        });
    }
    handleChange2(value){
        this.setState({
            village : value
        });
    }

    render(){
        console.log('village anm list', this.props.isFetching)
        return(
            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            {this.props.isFetching ? <div className='spin-sk'><Spin/></div> :
                <Row>
                <h2>Link Village with Anganbadis'</h2>
                <Form layout="inline" onSubmit={this.handlesubmit}>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select Villages </h4>
                        <Select onChange={this.handleChange2} style={{width : "250px"}} placeholder="Select a SMO">
                            {this.props.villageIcdsList.map((item) =>(
                                <Option key={item} value={item}>{item}</Option>
                            ))}
                        </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Select Anganbadis</h4>
                    <Select
                    mode="multiple"
                    style={{width : "250px"}}
                    placeholder="Please select Villages"
                    // defaultValue={['a10', 'c12']}
                    onChange={this.handleChange}>
                        {this.props.anganbadisList.map((item) =>(
                            <Option key={item}>{item}</Option>
                        ))}
                    </Select>
                    </FormItem>
                    </Col>
                    <Col span={8}>
                    <FormItem>
                    <h4>Submit</h4>
                        <Button
                        type="primary"
                        htmlType="submit"
                        >
                        Link Village with Anganbadis' Region
                        </Button>
                    </FormItem>
                    </Col>
                </Form>
                </Row>
            }
            </Content>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        isFetching : state.listFetchReducer.isFetching,
        anganbadisList : state.listFetchReducer.anganbadis,
        villageIcdsList : state.listFetchReducer.villages_icds
    }
  }
  
  export default connect(mapStateToProps)(LinkVillageAnganbadi)