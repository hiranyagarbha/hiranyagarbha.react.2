import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { login } from '../../action/login.action';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading : false,
            iconLoading: false,
            loginstatus : false,
            role : null,
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
            this.props.form.validateFields((err, values) => {
                if (!err) {
                console.log('login in')
                this.props.dispatch(login(values));
                console.log('login out')
                console.log('Received values of form: ', values);
                }
        });
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.status === true){
            this.setState({loginstatus : true})
        }
    }

    enterLoading = () => {
        this.setState({ loading: true });
    }

    // enterIconLoading = () => {
    //     this.setState({ iconLoading: true });
    // }

    render() {
        if ((this.state.loginstatus === true && this.props.role === "bmo") || (this.state.loginstatus === true && this.props.role === "cdpo")){
            return (<Redirect to='/dashboard'/>)
        }
        if (this.state.loginstatus === true && this.props.role === "superuser"){
            return (<Redirect to='/add-roles'/>)
        }

        const { getFieldDecorator } = this.props.form;
        return (
        <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
            {getFieldDecorator('email_or_username', {
                rules: [{ required: true, message: 'Please input your username!' }],
            })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" style={{height : '48px'}} />
            )}
            </FormItem>
            <FormItem>
            {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
            })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" style={{height : '48px'}} />
            )}
            </FormItem>
            <FormItem>
            {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
            })(
                <Checkbox style={{color : 'white'}}>Remember me</Checkbox>
            )}
            <a className="login-form-forgot" href="">Forgot password</a>
            <Button type="primary" htmlType="submit" className="login-form-button" style={{height : '42px',marginTop : '3%'}} loading={this.props.isFetching}>
                Log in
            </Button>
            {/* Or <a href="">register now!</a> */}
            </FormItem>
        </Form>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);


const mapStateToProps = (state) => {
    return{
        isFetching : state.loginAlerts.isFetching,
        status : state.loginAlerts.status,
        role : state.loginAlerts.role
    }
}
  
export default connect(mapStateToProps)(WrappedNormalLoginForm)