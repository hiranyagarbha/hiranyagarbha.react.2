import React from 'react';
import { Form, Icon, Input, Button,Layout } from 'antd';
import {connect} from 'react-redux';
import { UserCreation } from '../../action/user.create.action';
import { AuthHeader } from '../../helper/auth.token';


const {Content} = Layout
const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class HorizontalLoginForm extends React.Component {
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let url = 'https://enduring-grid-209608.appspot.com/users/users'
        this.props.dispatch(UserCreation(url , values , AuthHeader()))
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const userNameError = isFieldTouched('username') && getFieldError('username');
    const nameError = isFieldTouched('name') && getFieldError('name');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    const mobileError = isFieldTouched('mobile') && getFieldError('mobile');
    return (
        <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            <h2>Create Supervisors</h2>
            <Form layout="inline" onSubmit={this.handleSubmit}>
                <FormItem
                validateStatus={nameError ? 'error' : ''}
                help={nameError || ''}
                >
                <h4>Supervisor Name</h4>
                {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Please input your name!' }],
                })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="name" />
                )}
                </FormItem>
                <FormItem
                validateStatus={userNameError ? 'error' : ''}
                help={userNameError || ''}
                >
                <h4>Supervisor Username</h4>
                {getFieldDecorator('username', {
                    rules: [{ required: true, message: 'Please input your username!' }],
                })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                )}
                </FormItem>
                <FormItem
                    validateStatus={mobileError ? 'error' : ''}
                    help={mobileError || ''}
                >
                <h4>Supervisor Mobile No</h4>
                    {getFieldDecorator('mobile', {
                    rules: [{ required: true, message: 'Please input your mobile!' }],
                    })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Mobile" />
                    )}
                </FormItem>
                <FormItem
                validateStatus={passwordError ? 'error' : ''}
                help={passwordError || ''}
                >
                <h4>Supervisor Password</h4>
                {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Password" />
                )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('role' ,{
                    initialValue : 'icds'
                    })(
                    <Input style={{display : 'none'}} disabled={true} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text" placeholder="Password"/>
                    )}
                </FormItem>
                <FormItem>
                <h4>Submit</h4>
                <Button
                    type="primary"
                    htmlType="submit"
                    disabled={hasErrors(getFieldsError())}
                >
                    Create Supervisor
                </Button>
                </FormItem>
            </Form>
            <h5>* Use Mobile No as Password .Login credentials would be their Name & Mobile No</h5>
        </Content>
    );
  }
}

const CreateSupervisor = Form.create()(HorizontalLoginForm);

const mapStateToProps = (state) => {
    return{
        isFetching : state.loginAlerts.isFetching,
    }
}
  
export default connect(mapStateToProps)(CreateSupervisor)