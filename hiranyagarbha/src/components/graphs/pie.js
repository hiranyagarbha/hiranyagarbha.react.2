import React from 'react';
import {PieChart,Pie,Recharts,Sector} from 'recharts';
import { connect } from 'react-redux';
import { Select, Row, Col } from 'antd';

const Option = Select.Option;

const data = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300}];



class PieGraph extends React.Component {
    constructor(){
        super();
        this.handleSmoChange = this.handleSmoChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
    }


    handleTimeChange= (value)=>{
        console.log(`selected ${value}`);
    }

    handleSmoChange= (value)=>{
        console.log(`selected ${value}`);
    }


    render(){
        return(
            <blockquote>
            {this.props.role === "bmo" ? <div>
            <Row>
                <Col lg={12} md = {12} sm = {12} style={{textAlign: "center", borderRight : "1px dashed #f5f5f5"}}>
                <h5>SMO Filter</h5>
                    <Select title = "SMO Filter" defaultValue="lucy" mode="multiple" size='small' style={{ width: 120 }} onChange={this.handleSmoChange}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        <Option value="disabled" disabled>Disabled</Option>
                        <Option value="Yiminghe">yiminghe</Option>
                    </Select>
                </Col>
                <Col lg={12} md = {12} sm = {12} style={{textAlign: "center"}}>
                <h5>Date Filter</h5>
                    <Select defaultValue="Today" style={{ width: 120 }} size='small'  onChange={this.handleTimeChange}>
                        <Option value="Today">Today</Option>
                        <Option value="Last 7 Days">Last 7 Days</Option>
                        <Option value="Last Month">Last Month</Option>
                        <Option value="All">All</Option>
                    </Select>
                </Col>
            </Row>
            </div>:""
            }
            {this.props.role === "cdpo" ? <div>
            <Row>
                <Col lg={12} md = {12} sm = {12} style={{textAlign: "center" , borderRight : "1px dashed #f5f5f5"}}>
                <h5>Supervisor Filter</h5>
                    <Select title = "SMO Filter" defaultValue="lucy" mode="multiple" size='small' style={{ width: 120 }} onChange={this.handleSmoChange}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        <Option value="disabled" disabled>Disabled</Option>
                        <Option value="Yiminghe">yiminghe</Option>
                    </Select>
                </Col>
                <Col lg={12} md = {12} sm = {12} style={{textAlign: "center"}}>
                <h5>Date Filter</h5>
                    <Select defaultValue="Today" style={{ width: 120 }} size='small'  onChange={this.handleTimeChange}>
                        <Option value="Today">Today</Option>
                        <Option value="Last 7 Days">Last 7 Days</Option>
                        <Option value="Last Month">Last Month</Option>
                        <Option value="All">All</Option>
                    </Select>
                </Col>
            </Row>
            </div>:""
            }

            <PieChart width={200} height={250}>
                {/* <Pie data={data01} dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={50} fill="#8884d8" /> */}
                <Pie data={[{name: 'Group A', value: this.props.hr}, {name: 'Group B', value: this.props.nhr}]} dataKey="value" nameKey="name" cx="50%" cy="50%"  outerRadius={50} fill="#82ca9d" label />
            </PieChart>
            </blockquote>
        )
    }
}


const mapStateToProps = (state) => {
    return{
        isFetching : state.loginAlerts.isFetching,
        status : state.loginAlerts.status,
        role : state.loginAlerts.role,
        name : state.loginAlerts.name,
        district : state.loginAlerts.district,
        division : state.loginAlerts.division,
    }
  }
  
export default connect(mapStateToProps)(PieGraph)