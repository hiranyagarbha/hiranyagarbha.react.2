import React from 'react';
import { Table, Input, Button, Popconfirm, Form,Layout } from 'antd';
import { AuthHeader } from '../../helper/auth.token';
import axios from 'axios';

const {Content} = Layout
// const FormItem = Form.Item;

// export class EditableTable extends React.Component {
//   constructor(props) {
//     super(props);
//     this.columns = [{
//       title: 'BMO',
//       dataIndex: 'bmo',
//       width: '25%',
//     }, {
//       title: 'SMOs Name',
//       dataIndex: 'smo',
//       width: '25%',
//     }, {
//       title: 'ANMs Name',
//       dataIndex: 'anm',
//       width: '30%',
//     }, {
//       title: 'Linked Village',
//       dataIndex: 'village',
//       width: '25%',
//     }];

//     this.state = {
//       dataSource: [{
//         key: '0',
//         name: 'Edward King 0',
//         age: '32',
//         address: 'London, Park Lane no. 0',
//       }, {
//         key: '1',
//         name: 'Edward King 1',
//         age: '32',
//         address: 'London, Park Lane no. 1',
//       }],
//       count: 2,
//       data1 : [],
//     };
//   }

//   componentWillMount(){
//       axios.get( 'https://enduring-grid-209608.appspot.com/verify_linking', {headers: {Authorization : AuthHeader()}})
//       .then(response => {
//           console.log(response.data)
//           let d = response.data
//           this.setState({
//             data1 : d.links,
//           })
//       })
//       .catch(error =>{
//           console.log(error)
//       })
//   }

//   render() {
//     const { dataSource } = this.state;
//     const columns = this.columns.map((col) => {
//       if (!col.editable) {
//         return col;
//       }
//       return {
//         ...col,
//         onCell: record => ({
//           record,
//           editable: col.editable,
//           dataIndex: col.dataIndex,
//           title: col.title,
//           handleSave: this.handleSave,
//         }),
//       };
//     });
//     return (
//         <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
//             <div>
//                 <Button onClick={this.handleAdd} type="primary" style={{ marginBottom: 16 }}>
//                 Add a row
//                 </Button>
//                 <Table
//                 bordered
//                 dataSource={this.state.data1}
//                 columns={columns}
//                 />
//             </div>
//         </Content>
//     );
//   }
// }



// import { Table } from 'antd';
// import React from 'react';
// import { Table, Input, Button, Popconfirm, Form,Layout } from 'antd';
// import { AuthHeader } from '../../helper/auth.token';
// import axios from 'axios';

// const {Content} = Layout
// const FormItem = Form.Item;

// In the fifth row, other columns are merged into first column
// by setting it's colSpan to be 0
const renderContent = (value, row, index) => {
  const obj = {
    children: value,
    props: {},
  };
  if (index === 4) {
    obj.props.colSpan = 0;
  }
  return obj;
};

const columns = [{
  title: 'Name',
  dataIndex: 'name',
  render: (text, row, index) => {
    if (index < 4) {
      return <a href="javascript:;">{text}</a>;
    }
    return {
      children: <a href="javascript:;">{text}</a>,
      props: {
        colSpan: 5,
      },
    };
  },
}, {
  title: 'Age',
  dataIndex: 'age',
  render: renderContent,
}, {
  title: 'Home phone',
  colSpan: 2,
  dataIndex: 'tel',
  render: (value, row, index) => {
    const obj = {
      children: value,
      props: {},
    };
    if (index === 2) {
      obj.props.rowSpan = 2;
    }
    // These two are merged into above cell
    // if (data.tel[index] === data.tel[index-1]) {
    //   obj.props.colSpan = 0;
    // }
    if (index === 4) {
      obj.props.colSpan = 0;
    }
    return obj;
  },
}, {
  title: 'Phone',
  colSpan: 0,
  dataIndex: 'phone',
  render: renderContent,
}, {
  title: 'Address',
  dataIndex: 'address',
  render: renderContent,
}];

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  tel: '0571-22098909',
  phone: 18889898989,
  address: 'New York No. 1 Lake Park',
}, {
  key: '2',
  name: 'Jim Green',
  tel: '0571-22098333',
  phone: 18889898888,
  age: 42,
  address: 'London No. 1 Lake Park',
}, {
  key: '3',
  name: 'Joe Black',
  age: 32,
  tel: '0575-22098909',
  phone: 18900010002,
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '4',
  name: 'Jim Red',
  age: 18,
  tel: '0575-22098909',
  phone: 18900010002,
  address: 'London No. 2 Lake Park',
}, {
  key: '5',
  name: 'Jake White',
  age: 18,
  tel: '0575-22098909',
  phone: 18900010002,
  address: 'Dublin No. 2 Lake Park',
}];


export class EditableTable extends React.Component{


componentWillMount(){
    axios.get( 'https://enduring-grid-209608.appspot.com/verify_linking', {headers: {Authorization : AuthHeader()}})
    .then(response => {
        console.log(response.data)
        let d = response.data
        this.setState({
          data1 : d.links,
        })
    })
    .catch(error =>{
        console.log(error)
    })
}

  render(){
    return(
      <Table columns={columns} dataSource={data} bordered />
    )
  }
}
