import React from 'react';
import { Table,Layout } from 'antd';

const {Content} = Layout 

const columns = [{
  title: 'Patient Name',
  dataIndex: 'name',
  sorter: true,
  render: name => `${name.first} ${name.last}`,
  width: '20%',
}, {
  title: 'Assigned Doctor',
  dataIndex: 'gender',
  filters: [
    { text: 'Male', value: 'male' },
    { text: 'Female', value: 'female' },
  ],
  width: '20%',
}, {
  title: 'Assigned ANM',
  dataIndex: 'email',
},{
    title : 'Doctor',
    dataIndex : '',
},{
    title : 'Assigned Supervisor',
    dataIndex : '',
},{
    title : 'Status',
    dataIndex : '',
},{
    title: 'Reason',
    dataIndex : '',
},{
    title : 'Visits',
    dataIndex : '',
},{
    title : 'EDD',
    dataIndex : '',
},{
    title : 'Report',
    dataIndex : '',
    width : '20%'
}];

export class DashboardTable extends React.Component {
  state = {
    data: [],
    pagination: {},
    loading: false,
  };

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    pager.current = pagination.current;
    this.setState({
      pagination: pager,
    });
    this.fetch({
      results: pagination.pageSize,
      page: pagination.current,
      sortField: sorter.field,
      sortOrder: sorter.order,
      ...filters,
    });
  }

  fetch = (params = {}) => {
    console.log('params:', params);
    this.setState({ loading: true });
    // reqwest({
    //   url: 'https://randomuser.me/api',
    //   method: 'get',
    //   data: {
    //     results: 10,
    //     ...params,
    //   },
    //   type: 'json',
    // })
    // .then((data) => {
    //   const pagination = { ...this.state.pagination };
    //   // Read total count from server
    //   // pagination.total = data.totalCount;
    //   pagination.total = 200;
    //   this.setState({
    //     loading: false,
    //     data: data.results,
    //     pagination,  
    //   });
    // });
  }

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (

        <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
            <Table
                columns={columns}
                rowKey={record => record.login.uuid}
                dataSource={this.state.data}
                pagination={this.state.pagination}
                loading={this.state.loading}
                onChange={this.handleTableChange}
                scroll={{ x: 1300 }}
            />
        </Content>
    );
  }
}