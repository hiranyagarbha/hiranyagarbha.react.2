import React from 'react';
import { Layout, Collapse, Row, Col} from 'antd';
import PieGraph from '../graphs/pie';
import { CustomShapeBarChart } from '../graphs/bar.hrf';
import { AuthHeader } from '../../helper/auth.token';
import axios from 'axios';

const { Content } = Layout ;

const Panel = Collapse.Panel;

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};
const htm = (
    <div>
        <p>Sardi Mehta | 
        <span>Benu devi | </span>
        <span>High Risk | </span>
        </p>
        
    </div>
)

export class CollapsePatientDetails extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            tn : 0,
            nhr : 0,
            hr : 0,
        }
    }

    componentWillMount(){
        axios.get( 'https://enduring-grid-209608.appspot.com/dashboard_data', {headers: {Authorization : AuthHeader()}})
        .then(response => {
            console.log(response.data)
            let d = response.data
            this.setState({
                tn : d.total_number,
                nhr : d.not_high_risk,
                hr : d.high_risk,
                fetch : false,
            })
        })
        .catch(error =>{
            console.log(error)
        })
    }
    
    render(){
        return(
            <Content style={{ margin: '24px 16px', background: '#fff' }} >
                {/* <Collapse bordered={false} defaultActiveKey={['1']} >
                    <Panel header={htm} key="1" style={customPanelStyle}>
                        <p>{text}</p>
                    </Panel>
                    <Panel header="This is panel header 2" key="2" style={customPanelStyle}>
                        <p>{text}</p>
                    </Panel>
                    <Panel header="This is panel header 3" key="3" style={customPanelStyle}>
                        <p>{text}</p>
                    </Panel>
                </Collapse> */}
                <Row>
                    <Col xs={{ span: 5, offset: 0 }} lg={{ span: 8, offset: 0 }}  xs={{ span: 8, offset: 0 }} className='reg-hgb'>
                        <h3>Total Registrations</h3>
                        <h2 >{this.state.tn}</h2>
                    </Col>
                    <Col xs={{ span: 5, offset: 0 }} lg={{ span: 8, offset: 0 }}  xs={{ span: 8, offset: 0 }} className='reg-hgb , reg-hgb-2'>
                        <h3>Total High Risk Cases</h3>
                        <PieGraph hr = {this.state.hr} nhr ={this.state.nhr} />
                    </Col>
                    <Col xs={{ span: 5, offset: 0 }} lg={{ span: 8, offset: 0 }}  xs={{ span: 8, offset: 0 }} className='reg-hgb' >
                        <h3>High RiskCase Parameters </h3>
                        <CustomShapeBarChart/>
                    </Col>
                </Row>
                <Row>
                    <Col xs={{ span: 5, offset: 0 }} lg={{ span: 8, offset: 0 }}  xs={{ span: 8, offset: 0 }} className='reg-hgb' >
                        <h3>Total High Risk Cases</h3>
                    </Col>
                    <Col xs={{ span: 5, offset: 0 }} lg={{ span: 8, offset: 0 }}  xs={{ span: 8, offset: 0 }} className='reg-hgb , reg-hgb-2' >
                        <h3>Total Deliveries (EDD) </h3>
                    </Col>
                    <Col xs={{ span: 5, offset: 0 }} lg={{ span: 8, offset: 0 }}  xs={{ span: 8, offset: 0 }} className='reg-hgb'>
                        <h3>High RiskCase Parameters </h3>
                    </Col>
                </Row>
            </Content>
        )
    }
}
