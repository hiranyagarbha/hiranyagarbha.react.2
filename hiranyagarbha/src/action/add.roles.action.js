import axios from 'axios';
import { message } from 'antd';
import { addRoleAlert } from '../constants/add.roles.constant';

export const addRoleRequest = () =>({
    type : addRoleAlert.ADD_ROLE_REQUEST
})

export const addRoleSuccess = (success) => ({
    type : addRoleAlert.ADD_ROLE_SUCCESS,
    success : success
})

export const addRoleError = (error) => ({
    type : addRoleAlert.ADD_ROLE_ERROR,
    error
})


export const AddRoles = (url , userCredentials, AuthStr) => (dispatch) => {
    console.log('user creation entered')
    dispatch(addRoleRequest());
    message.loading('Creating User',1.5);
    axios.post(url,userCredentials , {headers: {Authorization : AuthStr}})
        .then(response => {
            if(response.status === 200 || response.status === 201){
                dispatch(addRoleSuccess(response));
                message.success('Created Successfully', 2.5)
            }
            else{
                dispatch(addRoleError(response.status))
                message.error('Something Went Wrong. Try Again !!')
            }
        })
        .catch(err =>{
            dispatch(addRoleError(err));
            message.error('Something Went Wrong. Try Again !!',2)
        })
}
