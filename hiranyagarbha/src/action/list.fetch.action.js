import axios from 'axios';
import { message } from 'antd';
import { listFetchConstant } from '../constants/fetch.constant';


export const listFetchRequest = () =>({
    type : listFetchConstant.LIST_FETCH_REQUEST
})

export const listFetchSuccess = (success) => ({
    type : listFetchConstant.LIST_FETCH_SUCCESS,
    success : success
})

export const listFetchError = (error) => ({
    type : listFetchConstant.LIST_FETCH_ERROR,
    error
})


export const listFetch = (url, AuthStr) => (dispatch) => {
    console.log('list fetch entered')
    dispatch(listFetchRequest());
    message.loading('Fetching List',1.5);
    axios.get(url, {headers: {Authorization : AuthStr}})
        .then(response => {
            if(response.status === 200){
                dispatch(listFetchSuccess(response));
                message.success('List Loaded Successfully', 2.5)
            }
            else{
                dispatch(listFetchError(response.status))
                message.error('Something Went Wrong. Reload Again !!')
            }
        })
        .catch(err =>{
            dispatch(listFetchError(err));
            message.error('Something Went Wrong. Try Again !!',2)
        })
}


const shouldFetchData = getState => {
    let fetchedData  = getState.listFetchReducer
    if(fetchedData.smo_list === []){
        return true  
    }
    else{
        return false
    }
}

export const fetchIfNeeded = (url , AuthStr) => (dispatch, getState) => {
    if (shouldFetchData(getState)) {
      return dispatch(listFetch(url, AuthStr))
    }
  }