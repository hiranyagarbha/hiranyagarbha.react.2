import axios from 'axios';
import { message } from 'antd';
import { addPatientAlert } from '../constants/add.patient.constant';

export const addPatientRequest = () =>({
    type : addPatientAlert.ADD_PATIENT_REQUEST
})

export const addPatientSuccess = (success) => ({
    type : addPatientAlert.ADD_PATIENT_SUCCESS,
    success : success
})

export const addPatientError = (error) => ({
    type : addPatientAlert.ADD_PATIENT_ERROR,
    error
})


export const addPatient = (url , userCredentials, AuthStr) => (dispatch) => {
    console.log('user creation entered')
    dispatch(addPatientRequest());
    message.loading('Adding Patient',1.5);
    axios.post(url,userCredentials , {headers: {Authorization : AuthStr}})
        .then(response => {
            if(response.status === 200 || response.status === 201){
                dispatch(addPatientSuccess(response));
                message.success('Added Successfully', 2.5)
            }
            else{
                dispatch(addPatientError(response.status))
                message.error('Something Went Wrong. Try Again !!')
            }
        })
        .catch(err =>{
            dispatch(addPatientError(err));
            message.error('Something Went Wrong. Try Again !!',2)
        })
}
