import axios from 'axios';
import { message } from 'antd';
import { userCreateAlert } from '../constants/user.alert';

export const userCreateRequest = () =>({
    type : userCreateAlert.USER_CREATE_REQUEST
})

export const userCreateSuccess = (success) => ({
    type : userCreateAlert.USER_CREATE_SUCCESS,
    success : success
})

export const userCreateError = (error) => ({
    type : userCreateAlert.USER_CREATE_ERROR,
    error
})


export const UserCreation = (url , userCredentials, AuthStr) => (dispatch) => {
    console.log('user creation entered')
    dispatch(userCreateRequest());
    message.loading('Creating User',1.5);
    axios.post(url,userCredentials , {headers: {Authorization : AuthStr}})
        .then(response => {
            if(response.status === 200 || response.status === 201){
                dispatch(userCreateSuccess(response));
                message.success('Created Successfully', 2.5)
            }
            else{
                dispatch(userCreateError(response.status))
                message.error('Something Went Wrong. Try Again !!')
            }
        })
        .catch(err =>{
            dispatch(userCreateError(err));
            message.error('Something Went Wrong. Try Again !!',2)
        })
}
