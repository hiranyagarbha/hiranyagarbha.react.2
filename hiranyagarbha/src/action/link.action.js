import axios from 'axios';
import { message } from 'antd';
import { linkConstant } from '../constants/link.constant';


export const linkRequest = () =>({
    type : linkConstant.LINK_REQUEST
})

export const linkSuccess = (success) => ({
    type : linkConstant.LINK_SUCCESS,
    success : success
})

export const linkError = (error) => ({
    type : linkConstant.LINK_ERROR,
    error
})


export const link = (url,credentials, AuthStr) => (dispatch) => {
    console.log('liNKING  entered')
    dispatch(linkRequest());
    message.loading('Linking Lists',1.5);
    axios.post(url,credentials, {headers: {Authorization : AuthStr}})
        .then(response => {
            if(response.status === 200){
                dispatch(linkSuccess(response));
                message.success('Linked Successfully', 2.5)
            }
            else{
                dispatch(linkError(response.status))
                message.error('Something Went Wrong. Try Again !!')
            }
        })
        .catch(err =>{
            dispatch(linkError(err));
            message.error('Something Went Wrong. Try Again !!',2)
        })
}
