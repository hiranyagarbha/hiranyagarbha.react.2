import { fetchConstant } from '../constants/fetch.constant'
import axios from 'axios';
import { alertConstant } from '../constants/alert.constant';
import { message } from 'antd';
import { historyHGB } from '../helper/history';

export const fetchAbort = () => ({
    type : fetchConstant.ABORT,
})

export const fetchError = error => ({
    type : fetchConstant.ERROR,
    error
})

export const fetchStart = () => ({
    type : fetchConstant.START,
})

export const fetchUpdate = payload => ({
    type : fetchConstant.UPDATE,
    payload
})

export const loginRequest = () =>({
    type : alertConstant.LOGIN_REQUEST
})

export const loginSuccess = (success) => ({
    type : alertConstant.LOGIN_SUCCESS,
    success : success
})

export const loginError = (error) => ({
    type : alertConstant.LOGIN_ERROR,
    error
})

export const logoutRequest = () => ({
    type : alertConstant.LOGOUT,
})


const updateFetch = (url) => (dispatch,getState) => {
    const timeSinceLastFetch = getState();

    const isDataStale = Date.now() - timeSinceLastFetch > 34 

    if(isDataStale){
        dispatch(fetchStart());
        axios.get(url)
            .then(response =>{
                dispatch(fetchUpdate(response));
            })
            .catch(err =>{
                dispatch(fetchError(err))
            })
    }
    else{
        dispatch(fetchAbort());
    }

}

export const login = (userCredentials) => (dispatch) => {
    console.log('login entered')
    dispatch(loginRequest());
    message.loading('Authenticating User',1.5);
    let url = 'https://enduring-grid-209608.appspot.com/users/login'
    axios.post(url,userCredentials)
        .then(response => {
            if(response.status === 200){
                console.log(response);
                dispatch(loginSuccess(response));
                message.success('Login Successfull', 2.5)
            }
            else{
                dispatch(loginError(response.status))
                message.error('Wrong User Credentials')
            }
        })
        .catch(err =>{
            dispatch(loginError(err));
            message.error('Wrong User Credentials',2)
        })
}

export const logout = () => (dispatch) => {
    console.log('logout entered')
    dispatch(logoutRequest());
    historyHGB.push('/login');
    message.success('Logout Successfull', 2.5)
}

// export const checkLoginStatus = () => (dispatch,getState) => {
//     const 
// }