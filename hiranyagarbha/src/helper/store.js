import {createStore , applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import { rootReducer } from '../reducer';


const loggerMiddleware = createLogger();

export const store = createStore(
    rootReducer,
    compose(
    applyMiddleware(
        thunk,
        loggerMiddleware
    ),window.devToolsExtension ? window.devToolsExtension() : f => f
    )
)