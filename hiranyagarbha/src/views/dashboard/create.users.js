import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import CreateSMO from '../../components/forms/create.SMO';
import CreateANM from '../../components/forms/create.ANM';
import CreateVillage from '../../components/forms/create.village';

export class CreateSMOView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <CreateSMO/>
                <CreateANM/>
                <CreateVillage/>
            </DashboardLayout>
        )
    }
}