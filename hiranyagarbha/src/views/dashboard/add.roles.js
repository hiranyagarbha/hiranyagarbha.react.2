import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import AddRole from '../../components/forms/add.roles';
import { DashboardTable } from '../../components/table/role.table';

export class AddRoleView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <AddRole/>
                <DashboardTable/>
            </DashboardLayout>
        )
    }
}