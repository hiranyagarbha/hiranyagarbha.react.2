import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import CreateSupervisor from '../../components/forms/create.supervisor';
import CreateAnganbadi from '../../components/forms/create.anganbadi';
import CreateAnganbadiRegion from '../../components/forms/create.anganbadi.region';

export class CreateUsersIcdsView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <CreateSupervisor/>
                <CreateAnganbadi/>
                {/* <CreateAnganbadiRegion/> */}
            </DashboardLayout>
        )
    }
}