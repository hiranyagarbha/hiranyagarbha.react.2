import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import { CollapsePatientDetails } from '../../components/collapse/collapse';
import { DashboardTable } from '../../components/table/table';
import {Route} from 'react-router-dom';
import { CreateSMO } from '../../components/forms/create.SMO';

export class DashboardView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <CollapsePatientDetails/>
                <DashboardTable/>
            </DashboardLayout>
        )
    }
}