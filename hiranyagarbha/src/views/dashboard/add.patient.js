import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import CreatePatients from '../../components/forms/add.patient';

export class CreatePatientView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <CreatePatients/>
            </DashboardLayout>
        )
    }
}