import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import CreateAnganbadi from '../../components/forms/create.anganbadi';

export class CreateAnganbadiView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <CreateAnganbadi/>
            </DashboardLayout>
        )
    }
}