import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import CreatePatients from '../../components/forms/add.patient';
import { DashboardTable } from '../../components/table/role.table';

export class RoleListView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <DashboardTable/>
            </DashboardLayout>
        )
    }
}