import React from 'react';
import DashboardLayout from '../../layout/dashboard';
import { EditableTable } from '../../components/table/link.table';
import LinkSupervisorVillage from '../../components/forms/link.supervisor.village';
import LinkVillageAnganbadi from '../../components/forms/link.village.anganbadi';


export class LinkSupervisorVillageAnganbadiWorkersView extends React.Component{
    render(){
        return(
            <DashboardLayout>
                <LinkSupervisorVillage/>
                <LinkVillageAnganbadi/>

                {/* <EditableTable/> */}
            </DashboardLayout>
        )
    }
}