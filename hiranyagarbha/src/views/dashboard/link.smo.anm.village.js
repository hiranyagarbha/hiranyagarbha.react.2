import React from 'react';
import LinkSmoAnm from '../../components/forms/link.smo.anm';
import LinkAnmVillage from '../../components/forms/link.anm.village';
import DashboardLayout from '../../layout/dashboard';
import { EditableTable } from '../../components/table/link.table';


export class LinkSmoAnmVillageView extends React.Component{
    render(){
        return(
            <DashboardLayout>
                <LinkSmoAnm/>
                <LinkAnmVillage/>
                <EditableTable/>
            </DashboardLayout>
        )
    }
}