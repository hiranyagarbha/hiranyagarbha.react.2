import React from 'react';
import {Row, Col } from 'antd';
import WrappedNormalLoginForm from '../../components/forms/login';


export class LoginView extends React.Component {
    render(){
        var bg=require('../../assets/img/bg-img-compressor.png')
        var mplogo=require('../../assets/img/mp-gov-logo.png')
        return(
            <Row type='flex' justify='start' className='login-row-bg-sk' style={{backgroundImage:'url('+bg+')'}}>
                <Col span={8} className='login-col-sk'>
                    <Row type='flex'>
                        <Col span={12} push={4} style={{marginTop: '10%'}} >
                            <img src={mplogo} className='mp-gov-logo-sk' alt='backgroundimage'/>
                        </Col>
                        <Col span={16} push={4} className='heading-div-sk'>
                            <span className='headline-1-sk'>Official portal for </span>
                            <br/>
                            <span className='headline-2-sk'>Hiranyagarbha</span>
                        </Col>
                        <Col span={16} push={4}>
                            <WrappedNormalLoginForm/>
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}