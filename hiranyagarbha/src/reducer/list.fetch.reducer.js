import { listFetchConstant } from '../constants/fetch.constant';


const InitialState = {
    error : null,
    isFetching : false,
    lastFetched : 0,
    smo_list : [],
    anm_list : [],
    village_list : [],
    supervisor : [],
    anganbadis : [],
    villages_icds : [],
    workers : []
}

export const listFetchReducer = ( state = InitialState , action ) => {
    switch(action.type){
        case listFetchConstant.LIST_FETCH_REQUEST :
            return {
                ...state,
                isFetching : true ,
            }
        case listFetchConstant.LIST_FETCH_SUCCESS:
            return {
                ...state,
                isFetching : false,
                lastFetched : Date.now(),
                smo_list: action.success.data.smo_list,
                anm_list: action.success.data.anm_list,
                village_list : action.success.data.village_list,
                supervisor : action.success.data.supervisors,
                anganbadis : action.success.data.anganbadis,
                villages_icds : action.success.data.villages,
                workers : action.success.data.workers
            }
        case listFetchConstant.LIST_FETCH_ERROR :
            return {
                ...state,
                isFetching : false,
                error : action.error
            }
        default :
            return state 
    }
}

