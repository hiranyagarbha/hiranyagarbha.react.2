import { addPatientAlert } from '../constants/add.patient.constant';


const InitialState = {
    error : null,
    isFetching : false,
    lastFetched : 0,
}

export const addPatientReducer = ( state = InitialState , action ) => {
    switch(action.type){
        case addPatientAlert.ADD_PATIENT_REQUEST :
            return {
                ...state,
                isFetching : true ,
            }
        case addPatientAlert.ADD_PATIENT_SUCCESS :
            return {
                ...state,
                isFetching : false,
                lastFetched : Date.now(),
                response: action.success.data
            }
        case addPatientAlert.ADD_PATIENT_ERROR :
            return {
                ...state,
                isFetching : false,
                error : action.error
            }
        default :
            return state 
    }
}

