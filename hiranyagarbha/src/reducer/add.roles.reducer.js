import { addRoleAlert } from '../constants/add.roles.constant';


const InitialState = {
    error : null,
    isFetching : false,
    lastFetched : 0,
}

export const addRoleReducer = ( state = InitialState , action ) => {
    switch(action.type){
        case addRoleAlert.ADD_ROLE_REQUEST :
            return {
                ...state,
                isFetching : true ,
            }
        case addRoleAlert.ADD_ROLE_SUCCESS:
            return {
                ...state,
                isFetching : false,
                lastFetched : Date.now(),
                response: action.success.data
            }
        case addRoleAlert.ADD_ROLE_ERROR :
            return {
                ...state,
                isFetching : false,
                error : action.error
            }
        default :
            return state 
    }
}

