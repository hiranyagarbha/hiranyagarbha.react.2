import { userCreateAlert } from '../constants/user.alert';


const InitialState = {
    error : null,
    isFetching : false,
    lastFetched : 0,
}

export const userCreateReducer = ( state = InitialState , action ) => {
    switch(action.type){
        case userCreateAlert.USER_CREATE_REQUEST :
            return {
                ...state,
                isFetching : true ,
            }
        case userCreateAlert.USER_CREATE_SUCCESS:
            return {
                ...state,
                isFetching : false,
                lastFetched : Date.now(),
                response: action.success.data
            }
        case userCreateAlert.USER_CREATE_ERROR :
            return {
                ...state,
                isFetching : false,
                error : action.error
            }
        default :
            return state 
    }
}

