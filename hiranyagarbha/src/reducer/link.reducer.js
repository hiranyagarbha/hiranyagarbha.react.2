import { linkConstant } from '../constants/link.constant';


const InitialState = {
    error : null,
    isFetching : false,
    lastFetched : 0,
}

export const linkReducer = ( state = InitialState , action ) => {
    switch(action.type){
        case linkConstant.LINK_REQUEST :
            return {
                ...state,
                isFetching : true ,
            }
        case linkConstant.LINK_SUCCESS:
            return {
                ...state,
                isFetching : false,
                lastFetched : Date.now(),
                response : action.success.data,
            }
        case linkConstant.LINK_ERROR :
            return {
                ...state,
                isFetching : false,
                error : action.error
            }
        default :
            return state 
    }
}

