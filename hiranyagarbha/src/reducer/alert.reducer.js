import { alertConstant } from '../constants/alert.constant';


const InitialState = {
    status : false,
    role : null,
    error : null,
    isFetching : false,
    lastFetched : 0,
    auth : null,
    anganbadi : [],
}

export const loginAlerts = ( state = InitialState , action ) => {
    switch(action.type){
        case alertConstant.LOGIN_REQUEST :
            return {
                ...state,
                isFetching : true ,
            }
        case alertConstant.LOGIN_SUCCESS :
            return {
                ...state,
                isFetching : false,
                lastFetched : Date.now(),
                role : action.success.data.role,
                auth : action.success.data.token,
                status : true,
                state : action.success.data.state,
                division : action.success.data.division,
                district : action.success.data.district,
                name : action.success.data.name,
                smo : action.success.data.smo,
                anm : action.success.data.anm,
                anganbadi : action.success.data.agbdi,
            }
        case alertConstant.LOGIN_ERROR :
            return {
                ...state,
                isFetching : false,
                error : action.error
            }
        case alertConstant.LOGOUT :
            return {
                ...state,
                status : false,
                auth : null,
                role : null,
                lastFetched : null,
                logoutTime : Date.now()
            }
        default :
            return state 
    }
}

