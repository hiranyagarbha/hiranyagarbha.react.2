import {combineReducers} from 'redux';
import {loginAlerts} from './alert.reducer';
import {userCreateReducer} from './user.create.reducer';
import {listFetchReducer} from './list.fetch.reducer';
import {linkReducer} from './link.reducer'

export const rootReducer = combineReducers({
    loginAlerts,userCreateReducer,listFetchReducer,linkReducer
})
